#!/usr/bin/env bash

# mkdir /var/www/pn_forests_portal
mkdir -p /var/www/pn_forests_portal
cp -r pn_forests_portal_easy_install /var/www/pn_forests_portal/
cd /var/www/pn_forests_portal

# Update repositories.
apt-get update

# Install git
apt-get install git

# Install pip3
apt-get install python3-pip

# Install setuptools
pip3 install setuptools

# Install and configure Nginx
apt-get install nginx
cp pn_forests_portal_easy_install/conf_files/nginx.conf /etc/nginx/nginx.conf
fuser -k 80/tcp
nginx

# Install Gunicorn
pip3 install gunicorn
cp pn_forests_portal_easy_install/conf_files/gunicorn_start.sh /var/www/pn_forests_portal/gunicorn_start.sh
chmod +x /var/www/pn_forests_portal/gunicorn_start.sh

# Install and configure Postgres/PostGIS
# First install gdal
add-apt-repository ppa:ubuntugis/ubuntugis-unstable
sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt trusty-pgdg main" >> /etc/apt/sources.list'
wget --quiet -O - http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | sudo apt-key add -
apt-get update
apt-get install postgresql-9.4-postgis-2.1 postgresql-contrib-9.4 postgresql-server-dev-9.4 --fix-missing
su - postgres -c "psql -c \"ALTER ROLE postgres WITH PASSWORD 'postgres'\"" # Set postgres role password
su - postgres -c "psql -c 'CREATE DATABASE pn_forests_portal'" # Create pn_forests_portal database
su - postgres -c "psql -d pn_forests_portal -c 'CREATE EXTENSION postgis'" # Add postgis extension

# Replace postgresql configuration files
cp pn_forests_portal_easy_install/conf_files/pg_hba.conf /etc/postgresql/9.4/main/pg_hba.conf
cp pn_forests_portal_easy_install/conf_files/postgresql.conf /etc/postgresql/9.4/main/postgresql.conf
service postgresql restart

# Install Node.js
apt-get install nodejs npm
# Install topojson
npm install -g topojson
sh -c 'echo "export NODE_PATH=/usr/local/lib/node_modules" >> ~/.bashrc'
source ~/.bashrc

# Install Numpy
pip3 install numpy

# Download and install Pysoda
git clone https://gitlab.com:iac/iac/Pysoda.git
cd Pysoda
python3 setup.py install
cd ..

# Download pn_forest_portal
git clone https://gitlab.com/dimitri-justeau/pn_forests_portal.git
pip3 install -r pn_forests_portal/requirements.txt
cp pn_forests_portal/pn_forests_portal/settings_server.py pn_forests_portal/pn_forests_portal/settings.py

# Migrate database
python3 pn_forests_portal/manage.py migrate

# Load fixtures
python3 pn_forests_portal/manage.py loaddata sites

# Collect static files
python3 pn_forests_portal/manage.py collectstatic
